package Modele;

public class TimeManager {
	
	private int dureeStep = 1; 		//duree d'un step
	private int steps = 0;			//steps totaux
	
	private Fourmiliere fourmiliere;
	private Terrain terrain; 
	
	public TimeManager (Fourmiliere fourm, Terrain terr) {
		fourmiliere = fourm;
		terrain = terr;
	}
	
	private void step() {
		fourmiliere.step();
		terrain.step();
	}
	
	public void Update() throws InterruptedException {
		
		Visiteur visiteur = new Affichage();
		fourmiliere.applique(visiteur);
		
		while(true) {
			
			if(steps >= 1440*30) {
				dureeStep = 1000;
			}
			
			Thread.sleep(dureeStep);
			step();
			steps++;
			
			fourmiliere.applique(visiteur);
		}
	}
}
