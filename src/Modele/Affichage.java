package Modele;

import java.util.Iterator;
import java.util.List;

public class Affichage implements Visiteur {
	
	public void agitSur(Fourmi fourmi) {
		
		if(fourmi instanceof Ouvriere) {
			System.out.println("Role : Ouvri�re");
			System.out.println("Tache : " + ((Ouvriere) fourmi).GetTache());
			System.out.println("Est dehors : " + ((Ouvriere) fourmi).GetDehors());
			System.out.println("Temps Dehors : " + ((Ouvriere) fourmi).GetTempsDehors());
		} else if(fourmi instanceof Soldat) {
			System.out.println("Role : Soldat");
		} else if(fourmi instanceof Sexue) {
			System.out.println("Role : Sexu�");
		} else if(fourmi instanceof Reine) {
			System.out.println("Role : Reine");
		}
		
		System.out.println("Age : " + fourmi.GetAge() + " jour(s)");
		System.out.println("Steps : " + fourmi.GetSteps());
		System.out.println("Stade : " + fourmi.GetStade());
		System.out.println("Poids : " + fourmi.GetPoids());
		System.out.println("Position : (" + fourmi.GetPosX() + "; " + fourmi.GetPosY() + ")");
	}
	
	public void agitSur(Fourmiliere facade) {
		
		System.out.println("FACADE UPDATED");
		
		System.out.println("Stock Nourriture : " + facade.GetPoidsProies());
		facade.GetReine().applique(this);
		
		List<Fourmi> fourmis = facade.GetFourmis();
		Iterator<Fourmi> iter = fourmis.iterator();
		
	    while (iter.hasNext()) {
	    	System.out.println("--------------------------");
	    	Fourmi f = iter.next();
	    	f.applique(this);
	    }
		System.out.println("--------------------------");
	}

	@Override
	public void agitSur(Terrain facade) {
		System.out.println("FACADE");
		
		
		List<Case> cases = facade.GetMatrice();
		Iterator<Case> iter = cases.iterator();
		
	    while (iter.hasNext()) {
	    	System.out.println("--------------------------");
	    	Case c = iter.next();
	    	c.applique(this);
	    }
		System.out.println("--------------------------");
	}

	@Override
	public void agitSur(Case _case) {
		System.out.println("Coordonn�es : (" + _case.GetX() + "; " + _case.GetY() + ")");
		
		List<ElementVisuel> elements = _case.GetElements();
		Iterator<ElementVisuel> iter = elements.iterator();
		
	    while (iter.hasNext()) {
	    	ElementVisuel e = iter.next();
	    	System.out.println("ELEMENT");
	    	e.applique(this);
	    }
	}

	@Override
	public void agitSur(Proie proie) {
		System.out.println("Poids : " + proie.GetPoids());
		System.out.println("Est Vivant : " + proie.GetVivant());
	}

	@Override
	public void agitSur(Combat combat) {}
}