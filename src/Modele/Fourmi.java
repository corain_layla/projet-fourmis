package Modele;
public abstract class Fourmi extends ElementVisuel {

	private int STEPSINADAY = 1440;	//nombre de minutes dans un jour
	
	private int steps = 0;		//date
	private int age = 0;		//age en nombre de jours
	private float poids = 2f;	//poids initial (oeuf)
	private StadeVie stade = StadeVie.oeuf;
	
	private int posX = 25;
	private int posY = 25;
	
	public void step(Fourmiliere fourmiliere) {
		
		steps++;
		
		if(Math.floorMod(steps, STEPSINADAY) == 0) {
			//nouveau jour
			age++;
			SeNourrit(fourmiliere);
			EtatSuivant();
		}
	}	
	
	/**Une fourmi se nourrit ou non d'une quantit� d�pendant de son poids et de son stade de vie
	 * 
	 * @param fourmiliere Fourmiliere dans laquelle r�side cette fourmi
	 * @return 0 si la fourmi n'a pas besoin de se nourrir, -1 si le stock de nourriture �tait inssufisant, 1 si succ�s
	 */
	public int SeNourrit(Fourmiliere fourmiliere) {
		
		float coeff = 0;
		boolean seNourrit = false;
		
		//doit on se nourrir? et de combien ?
		if(stade.equals(StadeVie.larve)) {
			coeff = 1f;
			seNourrit = true;
		} else if (stade.equals(StadeVie.fourmi)) {
			coeff = 0.333f;
			seNourrit = true;
		}
		
		if (seNourrit) {
			if(fourmiliere.GetPoidsProies() >= (poids*coeff)) {
				//on peut se nourrir
				fourmiliere.SetPoidsProies(fourmiliere.GetPoidsProies()-(poids*coeff));
				return 1;
			} else {
				//on ne peut pas
				SetStade(StadeVie.mort);
				System.out.println("Une fourmi est morte !");
				return -1;
			}
		}
		
		//la fourmi ne se nourrit pas
		return 0;
	}
	
	/**Fait vieillir une fourmi
	 * 
	 * @return Le nouveau stade de vie de la fourmi
	 */
	public StadeVie EtatSuivant() {
		
		switch(age){
	       case 3: 
	           SetStade(StadeVie.larve);
	           SetPoids(10f);
	           return StadeVie.larve;
	       case 13:
	    	   SetStade(StadeVie.nymphe);
	    	   return StadeVie.nymphe;
	       case 30:
	    	   SetStade(StadeVie.fourmi);
	    	   SetPoids(2f);
	    	   return StadeVie.fourmi;
	       default:
	    	   if(!(this instanceof Reine)) {
	    		   if(age>913) {
	    				//meurs
	    			   SetStade(StadeVie.mort);
	    			   return StadeVie.mort;
	    		   }
	    	   } else {
	    		   if(age>3650) {
	    			   //meurs
	    			   SetStade(StadeVie.mort);
	    			   return StadeVie.mort;
	    		   }
	    	   }
	    	   //aucun changement
	    	   return GetStade();
	   }
	}
	
	//Getters & Setters
	public StadeVie GetStade() {
		return stade;
	}
	
	public void SetStade(StadeVie newStade) {
		stade = newStade;
	}
	
	public int GetSteps() {
		return steps;
	}
	
	public void SetSteps(int newSteps) {
		steps = newSteps;
	}
	
	public int GetPosX() {
		return posX;
	}
	
	public void SetPosX(int x) {
		posX = x;
	}
	
	public int GetPosY() {
		return posY;
	}
	
	public void SetPosY(int y) {
		posY = y;
	}
	
	public int GetStepsInADay() {
		return STEPSINADAY;
	}
	
	public int GetAge() {
		return age;
	}
	
	public void SetAge(int newAge) {
		age = newAge;
	}
	
	public float GetPoids() {
		return poids;
	}
	
	public void SetPoids(float newPoids) {
		poids = newPoids;
	}
	
	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}