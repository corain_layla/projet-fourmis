package Modele;

import java.awt.Color;

public class Ouvriere extends Fourmi {

	private int TEMPSDEHORSMAX = 600;
	
	private Tache tache;
	private boolean dehors = false;
	private int tempsDehors;	//en nombre de steps
	
	private Proie proiePorte;	//proie port�e
	
	public Ouvriere (Tache newTache) {
		tache = newTache;
	}
	
	@Override
	public void step(Fourmiliere fourmiliere) {
		
		if(!GetStade().equals(StadeVie.mort)) {
			
			SetSteps(GetSteps()+1);
		
			if(Math.floorMod(GetSteps(), GetStepsInADay()) == 0) {
				//nouveau jour
				SetAge(GetAge()+1);
				SeNourrit(fourmiliere);
				EtatSuivant();
			}
			
			if (!dehors) {
				SortDehors();
			} else if (tache.equals(Tache.chasse)) {
				BougeAleatoire();
			} else if (tache.equals(Tache.retour)) {
				RetourFourmiliere(fourmiliere);
			}
			
			MajTempsDehors();
		}
		
	}
	
	/**Mise � jour du temps pass� dehors. S'il d�passe un certain temps, le fourmi meurt
	 * 
	 */
	private void MajTempsDehors() {
		
		if(dehors) {
			tempsDehors++;
			if (tempsDehors >= TEMPSDEHORSMAX) {
				System.out.println("Step " + GetSteps() + " Une fourmi est morte");
				SetStade(StadeVie.mort);
			}
		}
	}
	
	/**
	 * Fait sortir la fourmi si elle est en �tat
	 */
	private void SortDehors() {
		
		if ((tache.equals(Tache.chasse)) && (GetStade().equals(StadeVie.fourmi))) {
			dehors = true;
			tempsDehors = 0;
			//System.out.println("Step " + GetSteps() + " Une fourmi est sortie");
		}
	}
	
	/**
	 * Mouvement al�atoire d'une fourmi sur 4 directions
	 */
	private void BougeAleatoire() {
		
		double rand = Math.random()*4;
		int x;
		int y;
		
		if (rand < 1.0) {
			x = -1;
			y = 0;
		} else if(rand < 2.0) {
			x = 0;
			y = -1;
		} else if(rand < 3.0) {
			x = 1;
			y = 0;
		} else {
			x = 0;
			y = 1;
		}
		
		if((GetPosX() + x < 50) && (GetPosY() + y < 50) && (GetPosX() + x > 0) && (GetPosY() + y > 0) ) {
			SetPosX(GetPosX() + x);
			SetPosY(GetPosY() + y);
		}
	}
	
	/**Mouvement automatique d'une fourmi afin qu'elle rentre � sa fourmiliere avec son butin
	 * 
	 * @param fourmiliere 
	 */
	private void RetourFourmiliere(Fourmiliere fourmiliere) {
		
		if(GetPosX()>25) {
			SetPosX(GetPosX()-1);
			
		} else if(GetPosX()<25) {
			SetPosX(GetPosX()+1);
			
		} else if(GetPosY()>25) {
			SetPosY(GetPosY()-1);
			
		} else if(GetPosY()<25){
			SetPosY(GetPosY()+1);
			
		} else if ((GetPosX()==25) && (GetPosY()==25)) {
			fourmiliere.AddToNourriture(proiePorte);
			proiePorte = null;
			tache = Tache.chasse;
		}
	}
	
	@Override
	public void StepCombat() {
		if (tache.equals(Tache.chasse)) {
			tache = Tache.combat;
			System.out.println("Une fourmi rejoint le combat");
		}
	}
	
	//Getters & Setters
	public Tache GetTache() {
		return tache;
	}
	
	public void SetTache(Tache newTache) {
		tache = newTache;
	}
	
	public boolean GetDehors() {
		return dehors;
	}
	
	public int GetTempsDehors() {
		return tempsDehors;
	}
	
	public Proie GetProiePorte() {
		return proiePorte;
	}

	public void SetProiePorte(Proie proie) {
		proiePorte = proie;
	}
	
	
	@Override
	public Color GetColor() {
		if (!GetStade().equals(StadeVie.mort)) {
			return new Color(0, 0, 255);
		} else {
			return Color.lightGray;
		}
	}
	
	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
