package Modele;

import java.util.ArrayList;
import java.util.List;

public class Fourmiliere extends ElementVisuel {
	
	private Reine reine;
	private List<Fourmi> fourmis = new ArrayList<Fourmi>();
	private List<Proie> proies = new ArrayList<Proie>();
	private float poidsProies = 100000f;

	public Fourmiliere (List<Fourmi> liste) {
		reine = new Reine();
		reine.SetAge(30);
		reine.SetStade(StadeVie.fourmi);
		fourmis = liste;
		
		SetPosX(25);
		SetPosY(25);
	}
	
	public void step() {
		reine.step(this);
		for(Fourmi fourmi : fourmis) {
			fourmi.step(this);
		}
	}
	
	public void AddToNourriture (Proie proie) {
		System.out.println("Proie ramen�e au nid: +" + proie.GetPoids() + " nourriture");
		poidsProies = poidsProies + proie.GetPoids();
		System.out.println("Total nourriture : " + poidsProies);
	}

	//Getters & Setters
	public Reine GetReine() {
		return reine;
	}

	public void SetReine(Reine newReine) {
		reine = newReine;
	}

	public List<Fourmi> GetFourmis() {
		return fourmis;
	}

	public void SetFourmis(List<Fourmi> newFourmis) {
		fourmis = newFourmis;
	}

	public List<Proie> GetProies() {
		return proies;
	}

	public void SetProies(List<Proie> newProies) {
		proies = newProies;
	}
	
	public float GetPoidsProies() {
		return poidsProies;
	}
	
	public void SetPoidsProies(float newPoids) {
		poidsProies = newPoids;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
