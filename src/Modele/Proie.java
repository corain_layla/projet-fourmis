package Modele;
import java.awt.Color;

public class Proie extends ElementVisuel {

	private float poids;
	private boolean vivant = true;
	
	public Proie (float newPoids, int x, int y) {
		poids = newPoids;
		SetPosX(x);
		SetPosY(y);
	}
	
	//Getters & Setters
	public float GetPoids() {
		return poids;
	}
	
	public void SetPoids(float newPoids) {
		poids = newPoids;
	}
	
	public boolean GetVivant() {
		return vivant;
	}
	
	public void SetVivant(boolean bool) {
		vivant = bool;
	}
	
	@Override
	public Color GetColor() {
		return new Color(255, 0, 0);
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
