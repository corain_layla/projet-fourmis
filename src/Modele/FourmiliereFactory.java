package Modele;

import java.util.ArrayList;
import java.util.List;

public class FourmiliereFactory {

	private List<Fourmi> fourmis = new ArrayList<Fourmi>();
	private int nbFourmis;
	
	/**Nombre de fourmis g�n�r�es par la factory
	 * 
	 * @param nb
	 */
	public FourmiliereFactory(int nb) {
		nbFourmis = nb;
	}
	
	/**G�n�re une fourmiliere compos�e de 70% d'ouvri�res, 20% de soldats, et 10% d'individus sexu�s � l'�tat de LARVES. Il faut 30 jours � ces larves pour devenir des fourmis productives.
	 * 
	 * @return Fourmili�re
	 */
	public Fourmiliere NewFourmiliere() {
		
		for(int i = 0; i<Math.abs(nbFourmis*0.70f);i++) {
			fourmis.add(new Ouvriere(Tache.chasse));
		}
		
		for(int i = 0; i<Math.abs(nbFourmis*0.20f);i++) {
			fourmis.add(new Soldat());
		}
		
		for(int i = 0; i<Math.abs(nbFourmis*0.10f);i++) {
			fourmis.add(new Sexue());
		}
		
		return new Fourmiliere(fourmis);
	}
	
	/**G�n�re une fourmiliere compos�e de 70% d'ouvri�res, 20% de soldats, et 10% d'individus sexu�s ADULTES
	 * 
	 * @return Fourmili�re
	 */
	public Fourmiliere GenererFourmiliere() {
		
		for(int i = 0; i<Math.abs(nbFourmis*0.70f);i++) {
			Fourmi fourmi = new Ouvriere(Tache.chasse);
			fourmi.SetAge(30);
			fourmi.SetStade(StadeVie.fourmi);
			fourmis.add(fourmi);
		}
		
		for(int i = 0; i<Math.abs(nbFourmis*0.20f);i++) {
			Fourmi fourmi = new Soldat();
			fourmi.SetAge(30);
			fourmi.SetStade(StadeVie.fourmi);
			fourmis.add(fourmi);
		}
		
		for(int i = 0; i<Math.abs(nbFourmis*0.10f);i++) {
			Fourmi fourmi = new Sexue();
			fourmi.SetAge(30);
			fourmi.SetStade(StadeVie.fourmi);
			fourmis.add(fourmi);
		}
		
		return new Fourmiliere(fourmis);
	}
}
