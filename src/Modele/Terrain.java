package Modele;

import java.util.ArrayList;
import java.util.List;

public class Terrain implements Element {
	
	private List<Case> matrice = new ArrayList<Case>();
	private List<ElementVisuel> elements = new ArrayList<ElementVisuel>();
	
	public void step() {
		
		for(Case _case : matrice) {
			_case.step(this);
		}
	}
	
	/**Ajoute un �l�ment � la liste des �l�ments du terrain
	 * 
	 * @param element � ajouter
	 */
	public void AddToElements(ElementVisuel element) {
		elements.add(element);
	}
	
	/**Retire un �l�ment de la liste des �l�ments du terrain
	 * 
	 * @param element � supprimer
	 */
	public void RemoveFromElements(ElementVisuel element) {
		elements.remove(element);
	}
	
	/**Met � jour un combat, bas� sur sa position.
	 * 
	 * @param combat
	 */
	public void MajCombat(Combat combat) {
		
		int i = 0;
		for(ElementVisuel element : elements) {
			if((element.GetPosX()==combat.GetPosX())&&(element.GetPosY()==combat.GetPosY()) && (element instanceof Combat)) {
				elements.remove(i);
				elements.add(combat);
			}
		}
	}
	
	//Getters & Setters
	public void SetMatrice(List<Case> m) {
		matrice = m;
	}
	
	public List<Case> GetMatrice() {
		return matrice;
	}
	
	public List<ElementVisuel> GetElements() {
		return elements;
	}
	
	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
