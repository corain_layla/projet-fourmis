package Modele;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Combat extends ElementVisuel{

	private boolean combat = false;
	
	private Proie proie;
	private List<Ouvriere> fourmis = new ArrayList<>();
	
	private int duree;
	private float poidsFourmis;
	
	public Combat(Proie p, List<Ouvriere> liste) {
		proie = p;
		fourmis = liste;
		duree = 0;
	}
	
	@Override
	public void StepCombat() {
		
		if (combat) {
			if(duree < 60*3) { //3heures MAX
				
				//combat continue
				combat = true;
				
				poidsFourmis = 0;
				for(Fourmi fourmi : fourmis) {
					poidsFourmis = poidsFourmis + fourmi.GetPoids();
				}
				
				if(poidsFourmis *2 >= proie.GetPoids()) {
					//proie meurt
					System.out.println("Proie tu�e !");
					combat = false;
					proie.SetVivant(false);
					for(Ouvriere fourmi : fourmis) {
						fourmi.SetTache(Tache.chasse);
					}
					fourmis.get(0).SetProiePorte(proie);
					fourmis.get(0).SetTache(Tache.retour);
				}
				
			} else {
				
				//combat timeout
				combat = false;
				System.out.println("Combat termin�");
				for(Ouvriere fourmi : fourmis) {
					fourmi.SetTache(Tache.chasse);
				}
			}
			duree++;
		}
	}
	
	/**Fonction de modification d'un combat
	 * 
	 * @param x 
	 * @param y 
	 * @param proie
	 * @param fourmis
	 * @param bool
	 */
	public void SetAll(int x, int y, Proie proie, List<Ouvriere> fourmis, boolean bool) {
		SetFourmis(fourmis);
		SetProie(proie);
		SetPosX(x);
		SetPosY(y);
		SetCombat(true);
	}

	//Getters & Setters
	public Proie GetProie() {
		return proie;
	}
	
	public void SetProie(Proie newProie) {
		proie = newProie;
	}
	
	public List<Ouvriere> GetFourmis() {
		return fourmis;
	}
	
	public void SetFourmis(List<Ouvriere> liste) {
		fourmis = liste;
	}
	
	public int GetDuree() {
		return duree;
	}
	
	public void SetDuree(int newDuree) {
		duree = newDuree;
	}
	
	public boolean GetCombat() {
		return combat;
	}
	
	public void SetCombat(boolean bool) {
		combat = bool;
	}
	
	@Override
	public Color GetColor() {
		return new Color(0, 255, 0);
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
