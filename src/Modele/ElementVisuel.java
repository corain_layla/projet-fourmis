package Modele;

import java.awt.Color;

public abstract class ElementVisuel implements Element {
	
	private int posX;
	private int posY;
	
	//Getters & Setters
	public int GetPosX() {
		return posX;
	}

	public void SetPosX(int posX) {
		this.posX = posX;
	}

	public int GetPosY() {
		return posY;
	}

	public void SetPosY(int posY) {
		this.posY = posY;
	}
	
	//M�thodes
	public void StepCombat() {}
	
	public Color GetColor() {
		return new Color(0, 0, 0);
	}
}