package Modele;

import java.util.ArrayList;
import java.util.List;

public class TerrainFactory {

	private List<Case> matrice = new ArrayList<Case>();
	private int DIMENSIONS = 50;
	private Fourmiliere fourmiliere;
	private List<Proie> proies = new ArrayList<Proie>();
	
	public TerrainFactory(Fourmiliere fourm) {
		fourmiliere = fourm;
	}
	
	/**Remplit la matrice de Cases d'un terrain d'une dimension de 100x100 cases, et y place des proies de fa�on al�atoire, ainsi que les fourmilieres et ses fourmis
	 * 
	 * @param terrain Terrain dont la matrice est � construire
	 */
	public void GenererTerrain(Terrain terrain) {
		
		for(int i=0;i<DIMENSIONS;i++) {
			for(int j=0;j<DIMENSIONS;j++) {
				matrice.add(new Case(i, j));
			}
		}
		
		terrain.SetMatrice(matrice);
		
		for(int i = 0; i<DIMENSIONS; i++) {
			double randX = Math.random()*DIMENSIONS;
			double randY = Math.random()*DIMENSIONS;
			
			proies.add(new Proie(3f, (int)randX, (int)randY));
		}
		
		for(Proie proie : proies) {
			terrain.AddToElements(proie);
		}
		
		for(Fourmi element : fourmiliere.GetFourmis()) {
			terrain.AddToElements(element);
		}
	}
}
