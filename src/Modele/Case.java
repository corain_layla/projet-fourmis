package Modele;

import java.util.ArrayList;
import java.util.List;

public class Case implements Element {

	private int posX;
	private int posY;
	
	private List<ElementVisuel> elements = new ArrayList<ElementVisuel>();
	
	public Case (int x, int y) {
		posX = x;
		posY = y;
	}
	
	public void step(Terrain terrain) {
		
		//mise � jour de la liste des �l�ments visuels
		MajElements(terrain.GetElements());
		
		Proie proie = null;
		List<Ouvriere> fourmis = new ArrayList<Ouvriere>();
		Combat combatObject = new Combat(proie, fourmis);
		
		//check fourmis + proie dans notre case
		for( ElementVisuel element : elements) {
			
			//une ouvriere dans la case
			if(((element instanceof Ouvriere)) && (!((Ouvriere) element).GetTache().equals(Tache.retour)) ) {
				fourmis.add((Ouvriere) element);
			}
			
			//une proie vivante dans la case
			if((element instanceof Proie) && (((Proie) element).GetVivant())) {
				proie = (Proie) element;
			}
			
			//un combat a lieu
			if(element instanceof Combat) {
				combatObject = (Combat) element;
			}
		}
			
		if(!combatObject.GetCombat()) {	
			
			//premier contact
			if ((fourmis.size() != 0) && (proie != null)) {
				
				//Creation d'un combat
				combatObject.SetAll(posX, posY, proie, fourmis, true);
				elements.add(combatObject);
				terrain.AddToElements(combatObject);
				
				System.out.println("Combat en (" + posX + "; " + posY + ") : ");
				for( ElementVisuel element : elements) {
					element.StepCombat();
				}
				
				//combat fini?
				CheckCombat(combatObject, proie, terrain);
			}
			
		} else {	//Combat en cours
			
			if ((fourmis.size() != 0) && (proie != null)) {
				//MaJ fourmis combattantes
				
				//Mise � jour du combat en cours
				combatObject.SetAll(posX, posY, proie, fourmis, true);
				terrain.MajCombat(combatObject);
				
				System.out.println("Combat en (" + posX + "; " + posY + ") : ");
				for( ElementVisuel element : elements) {
					element.StepCombat();
				}
				
				//combat fini?
				CheckCombat(combatObject, proie, terrain);
			}
		}
	}
	
	/*V�rifie si un combat est termin�. Si oui, efface le combat et la proie vaincue de l'univers
	 * 
	 */
	private void CheckCombat(Combat combatObject, Proie proie, Terrain terrain) {
				
		if (!combatObject.GetCombat()) {
			
			for(int i=0; i<elements.size(); i++) {
				if(elements.get(i).equals(proie)) {
					terrain.RemoveFromElements(elements.get(i));
					elements.remove(i);
					break;
				}
		    }
		    
			for(int i=0; i<elements.size(); i++) {
				if(elements.get(i).equals(combatObject)) {
					terrain.RemoveFromElements(elements.get(i));
					elements.remove(i);
					break;
				}
		    }
		}
	}
	
	/**Met � jour les �l�ments de cette case, bas�e sur une liste d'�lements complete : celle du terrain
	 * 
	 * @param liste
	 */
	private void MajElements(List<ElementVisuel> liste) {
		
		elements = new ArrayList<ElementVisuel>();
		for(ElementVisuel element : liste) {
			if((element.GetPosX() == posX) && (element.GetPosY() == posY)) {
				elements.add(element);
			}
		}
	}
	
	//Getters & Setters
	public List<ElementVisuel> GetElements() {
		return elements;
	}
	
	public int GetX() {
		return posX;
	}
	
	public int GetY() {
		return posY;
	}
	
	public void SetElements(List<ElementVisuel> newElements) {
		elements = newElements;
	}

	@Override
	public void applique(Visiteur unVisiteur) {
		unVisiteur.agitSur(this);
	}
}
