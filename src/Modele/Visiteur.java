package Modele;

public interface Visiteur {
	
	void agitSur(Fourmi fourmi);
	void agitSur(Fourmiliere facade);
	
	void agitSur(Terrain facade);
	void agitSur(Case _case);
	void agitSur(Proie proie);
	void agitSur(Combat combat);
}