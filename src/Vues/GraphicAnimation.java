package Vues;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import GraphicCore.NiRectangle;

class GraphicAnimation implements ActionListener {
	
	final int graphicAnimationDelay = 25;
	NiRectangle home;
	
	public GraphicAnimation(NiRectangle home) {
		this.home = home;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Component[] views =  this.home.getComponents();
		for (int i = 0; i < views.length; i++) {
			View next = (View) views[i];
			next.Update();
		}
	}
	
	public void start() {
		Timer animation = new Timer(0, this);
		animation.setDelay(this.graphicAnimationDelay);
		animation.start();
	}
}