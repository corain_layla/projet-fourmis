package Vues;

import java.awt.Color;
import java.awt.Dimension;
import GraphicCore.NiRectangle;
import GraphicCore.NiSpace;
import Modele.Fourmiliere;
import Modele.FourmiliereFactory;
import Modele.Terrain;
import Modele.TerrainFactory;
import Modele.Case;

public class MoveThings {
	
	private FourmiliereFactory factoryFourmiliere = new FourmiliereFactory(10);
	private TerrainFactory factoryTerrain;
	private Fourmiliere fourmiliere;
	private Terrain terrain = new Terrain();
	private NiRectangle home;
	NiSpace space = new NiSpace("Move things", new Dimension(800, 800));
	
	public MoveThings() {
		
		fourmiliere = factoryFourmiliere.GenererFourmiliere();
		factoryTerrain = new TerrainFactory(fourmiliere);
		factoryTerrain.GenererTerrain(terrain);
		
		home = new NiRectangle();
		home.setBounds(100, 100, 600, 600);
		home.setBackground(Color.lightGray);

		space.setDoubleBuffered(true);
		
		for(Case _case : terrain.GetMatrice()) {
			View view = new View(_case);
			this.home.add(view);
		}
		
		space.add(home);
		space.openInWindow();
	}
	
	public void startGraphicAnimation() {
		GraphicAnimation animation = new GraphicAnimation(home);
		animation.start();
	}
	
	private void step() {
		fourmiliere.step();
		terrain.step();
	}
	
	public void mainLoop() throws InterruptedException {
		
		int steps = 0;
		int DUREESTEP = 10;
		
		while(true) {
			
			home = new NiRectangle();
			home.setBounds(100, 100, 600, 600);
			home.setBackground(Color.lightGray);

			space.setDoubleBuffered(true);
			
			for(Case _case : terrain.GetMatrice()) {
				View view = new View(_case);
				this.home.add(view);
			}
			
			System.out.println("Step " + steps);
			Thread.sleep(DUREESTEP);
			
			step();
			steps++;
		}
	}
	
	public static void main(String args[]) {
		
		MoveThings movingThings = new MoveThings();
		movingThings.startGraphicAnimation();
		
		try {
			movingThings.mainLoop();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}