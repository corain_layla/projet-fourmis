package Vues;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import GraphicCore.NiRectangle;
import Modele.Case;
import Modele.ElementVisuel;

public class View extends NiRectangle {
	
	private int SIZECASE = 12;
	
	private static final long serialVersionUID = 8010266472160477056L;
	private Case _case;
	
	public View(Case c) {
		_case = c;
		this.setDimension(new Dimension(SIZECASE,SIZECASE));
		this.setLocation(new Point(_case.GetX()*SIZECASE, _case.GetY()*SIZECASE));
		
		this.setBackground(Color.lightGray);
	}
	
	/**
	 * Mise � jour contenu des cases
	 */
	public void Update() {
		
		//mettre � jour couleur des cases
		
		if(_case.GetElements().size() == 0) {
			this.setBackground(Color.lightGray);
			
		} else {
			
			//couleur du premier �l�ment visuel de la case
			ElementVisuel element = _case.GetElements().get(0);
			this.setBackground(element.GetColor());
			this.revalidate();
			this.repaint();
		}
	}
}
