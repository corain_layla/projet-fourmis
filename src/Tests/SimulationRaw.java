package Tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Modele.Fourmiliere;
import Modele.FourmiliereFactory;
import Modele.Terrain;
import Modele.TerrainFactory;
import Modele.TimeManager;

class SimulationRaw {

	FourmiliereFactory factoryFourmiliere = new FourmiliereFactory(10);
	TerrainFactory factoryTerrain;
	TimeManager timer;
	Fourmiliere fourmiliere;
	Terrain terrain = new Terrain();

	@Test
	void test() {
		fourmiliere = factoryFourmiliere.NewFourmiliere();
		factoryTerrain = new TerrainFactory(fourmiliere);
		factoryTerrain.GenererTerrain(terrain);
		
		timer = new TimeManager(fourmiliere, terrain);
		try {
			timer.Update();
		} catch (InterruptedException e) {
			fail(e);
		}
	}
}